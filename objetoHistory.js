let atras = document.getElementById('button_back');
let alante = document.getElementById("button-forward");

atras.addEventListener('click', atras);
alante.addEventListener('click', alante);

function atras(){
    history.back();
    return true;
}

function alante(){
    history.forward();
    return true;

}