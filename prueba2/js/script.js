/* Solicitar 5 números, comprobar si es un número entero positivo.
    Mostrarlos en orden creciente 
    
    1-Pedir números
    2-comprobar si es un número
    3-comprobar si es positivo
    4-guardar números
    5-ordenar números
    6-imprimir números
    */

let numbers = [];
/* Utilizo un while porque no sé cuantas vueltas va a dar hasta que el usuario meta los datos correctos 5 veces */
/* Si utilizara un for tendría que usar un else para restar 1 a i (i--) y conseguir otra vuelta */
/* Si la longitud del array es menor a 5 pido un número  */
while(numbers.length<5){
    let number = prompt("Introduzca un número");

    /* Compruebo si number es un número, si number es mayor a 0 y compruebo si number tiene un punto, si no encuentra el punto tiene que devolver -1 */
    if(!isNaN(number) && number>0 && number.indexOf('.')==-1){
        numbers.push(number); /* Guardo los números en el array */
    }
}

/* sort coge los elementos del array de dos en dos(a,b) compara a y b para saber cual es el mayor. Equivaldría a un for con un if */

numbers.sort((a,b)=>a-b); /* sort recibe dos parámetros, con la función de flecha hacemos un return automático(la flecha equivale al return). 
                            Se comrpueba si a-b es mayor o menor que 0, si 'a' es mayor los ordena de mayor a menor, si 'a' es menor los ordena de menor a mayor */

document.write(numbers);


