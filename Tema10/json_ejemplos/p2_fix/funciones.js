addEventListener('load',inicializarEventos,false);

function inicializarEventos() {
  var ob=document.getElementById('boton1');
  ob.addEventListener('click',presionBoton,false);
}

function presionBoton(e) {
  var ob=document.getElementById('dni');
  recuperarDatos(ob.value);
}


function recuperarDatos(dni) {
	var contenedor = document.getElementById("resultados");
	var objetoXHR;
	if (window.XMLHttpRequest) {
		objetoXHR = new XMLHttpRequest();
		} else {
		// IE6, IE5
		objetoXHR = new ActiveXObject("Microsoft.XMLHTTP");
	}

	objetoXHR.onreadystatechange=function() {
	  	if ( (objetoXHR.readyState==4) && (objetoXHR.status=200) ) {
			var datos=JSON.parse(objetoXHR.responseText);
	    	if (datos.apellido.length>0) {
	    		var salida = "Apellido: "+datos.apellido+"<br>";
	    		salida=salida+"Nombre: "+datos.nombre+"<br>";
	    		salida=salida+"Dirección: "+datos.direccion;
	    		contenedor.innerHTML = "<BR>" + salida;	
	    	}
	    	else contenedor.innerHTML = "<BR>" + "Número de DNI inexistente.";
		}
		else { 
	    	contenedor.innerHTML = "<BR>" + "Cargando...";
	  	}
  	}
  	objetoXHR.open('GET','pagina1.php?dni='+dni, true);
  	objetoXHR.send();
 }
