addEventListener('load',inicializarEventos,false);

function inicializarEventos() {
  var ob=document.getElementById('boton1');
  ob.addEventListener('click',presionBoton,false);
}

function presionBoton(e) {
  var objeto={
    'nombre':'juan',
    'edad':25,
    'sueldos':[1200,1700,1990]
  };
  var cadena=JSON.stringify(objeto);
  enviarDatos(cadena);
}


function enviarDatos(cadena) {
	var contenedor = document.getElementById("resultados");
	var objetoXHR;
	if (window.XMLHttpRequest) {
		objetoXHR = new XMLHttpRequest();
		} else {
		// IE6, IE5
		objetoXHR = new ActiveXObject("Microsoft.XMLHTTP");
	}

	objetoXHR.onreadystatechange=function() {
	  	if ( (objetoXHR.readyState==4) && (objetoXHR.status=200) ) {
			contenedor.innerHTML=objetoXHR.responseText;
		}
		else { 
	    	contenedor.innerHTML = "<BR>"+"Cargando...";
	  	}
  	}
  	objetoXHR.open('GET','pagina1.php?cadena='+cadena, true);
  	objetoXHR.send();
 }
