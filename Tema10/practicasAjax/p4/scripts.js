const container = document.getElementById('container')
const links = document.getElementById('links')

links.addEventListener('click', (e) => {
    if (e.target.tagName == 'A') {
        e.preventDefault()

        const xhr = new XMLHttpRequest()
        xhr.open('GET', e.target.href, true)
        
        xhr.addEventListener('load', data => {
            container.innerHTML = data.target.responseText
        })
        xhr.send()
    }
})