const form = document.getElementById('form')
const login = document.getElementById('login')
const response =  document.getElementById('response')

form.addEventListener('submit', e => {
    e.preventDefault()
    
    const xhr = new XMLHttpRequest()
    xhr.open('GET', `compruebaDisponibilidad.php?login=${login.value}`)

    xhr.addEventListener('load', data => {
        const dataJSON = JSON.parse(data.target.responseText)
        console.log(dataJSON);
        if (dataJSON.disponible == 'Si') {
            response.textContent = 'El nombre está disponible'
        } else {
            response.textContent = `El nombre no está disponible. Puedes usar ${dataJSON.alternativas}`
        }
        console.dir(data.target);
    })


    xhr.send()

})