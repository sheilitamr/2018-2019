const provincias = document.getElementById('provincias')
const municipios = document.getElementById('municipios')

addEventListener('DOMContentLoaded', e => {
    const xhr = new XMLHttpRequest()
    xhr.open('GET', 'cargarProvincias.php')
    xhr.addEventListener('load', data => {
        const json = JSON.parse(data.target.responseText)

        for (const prov of json){

            const option = document.createElement('OPTION')
            option.setAttribute('value', prov.id_provincia)
            option.textContent = prov.provincia
            provincias.appendChild(option)
        }
        console.log(xhr.readyState);
        console.log(xhr.status);
       
    })

    xhr.send() 
})

 provincias.addEventListener('change',(e)=>{
    if(e.target.value !='default'){
        console.log(e.target.value);
        seleMun(e.target.value)
    }
})

const seleMun =(idProvincia)=>{

    //console.log(idProvincia);
    const xhr = new XMLHttpRequest()
    xhr.open('GET',`cargarMunicipios.php?id_provincia=${idProvincia}`)
    xhr.addEventListener('load', data=>{

    const json = JSON.parse(data.target.responseText)

    municipios.innerHTML='<option value="default">Selecciona un municipio</option>'

    for(const mun of json){
        const option = document.createElement('OPTION')
        option.textContent = mun.nombre
        municipios.appendChild(option)
    }
})
    xhr.send()

}







