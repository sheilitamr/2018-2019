let ventana;

let crear = document.getElementById('crear');
let cerrar = document.getElementById('cerrar');

crear.addEventListener('click', CrearVentana);
cerrar.addEventListener('click', CerrarVentana);

function CrearVentana() {
    let width = document.getElementById('ancho1').value;
    let height = document.getElementById('alto1').value;
    let opciones = "left=100, top=100,";

    opciones = opciones + "width=" + width + ",";
    opciones = opciones + "height=" + height + ",";
    ventana = window.open("", "nom_ventana1", opciones);
    ventana.document.write("ESTA ES LA VENTANA QUE SE HA CREADO");
    ventana.document.write("<br>" + "Direccioón de la página que ha abierto esta ventana:");
    ventana.document.write("<br>" + ventana.opener.location);
}

function CerrarVentana() {
    if (ventana && !ventana.closed) 
        ventana.close();
    
}