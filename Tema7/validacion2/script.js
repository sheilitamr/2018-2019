function validate() {
    let error =false;
    let name = document.getElementById('id_name').value;
    let phone = document.getElementById('id_phone').value;
    let msg="";
    document.getElementById('mensaje_nombre').innerHTML="";
    document.getElementById('mensaje_telefono').innerHTML="";

    if (name.length == 0 || name == null) {
        msg="Este campo no debe estar vacío";
        document.getElementById('mensaje_nombre').innerHTML=msg;
        error = true;
        
  
    }

    if (isNaN(phone) || phone.length!=9) {
        msg="Este campo debe ser un número, no estar vacío y tener 9 dígitos";
        document.getElementById('mensaje_telefono').innerHTML=msg;
        error = true;
        
    }
    return !error;
}