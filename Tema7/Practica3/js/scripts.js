function validate() {
    let error = false;
    let msg = "";

    let age = document.getElementById("id_age").value;
    let gender = document.getElementsByName('gender');
    let studies = document.getElementById('id_studies');
    let computerGeneral = document.getElementsByName('computer');
    let daysSchool = document.getElementById('id_daysSchool');
    let hoursSchool = document.getElementById('id_hoursSchool');
    let daysHolidays = document.getElementById('id_daysHolidays');
    let hoursHolidays = document.getElementById('id_hoursHolidays');
    let activities = document.getElementById('id_activities');
    let typeComputer = document.getElementById('id_typeC');
    let timeComputer = document.getElementById('id_timeComputer');
    let brand = document.getElementById('id_selectBrand');

    document.getElementById('id_error').innerHTML = "";

    if (age <= 0 || age > 99) {
        msg = "edad erronea";
        document.getElementById('id_error').innerHTML = msg;
        error = true;
    }

    if (gender[0].checked || gender[1].checked) {
        msg = "Has seleccionado";
        document.getElementById('id_error').innerHTML = msg;
    } else {
        msg = "No has seleccionado nada";
        document.getElementById('id_error').innerHTML = msg;
        error = true;
    }

    if (studies.selectedIndex == 0) {
        msg = "Debe seleccionar una opción";
        document.getElementById('id_error').innerHTML = msg;
        error = true;
    }

    if (computerGeneral[0].checked || computerGeneral[1].checked) {
        msg = "Has seleccionado";
        document.getElementById('id_error').innerHTML = msg;
    } else {
        msg = "No has seleccionado nada";
        document.getElementById('id_error').innerHTML = msg;
        error = true;
    }

    if (daysSchool.selectedIndex == 0) {
        msg = "Debes seleccionar una opción";
        document.getElementById('id_error').innerHTML = msg;
        error = true;
    }

    if (hoursSchool.selectedIndex == 0) {
        msg = "Debes seleccionar una opción";
        document.getElementById('id_error').innerHTML = msg;
        error = true;
    }

    if (daysHolidays.selectedIndex == 0) {
        msg = "Debes seleccionar una opción";
        document.getElementById('id_error').innerHTML = msg;
        error = true;
    }

    if (hoursHolidays.selectedIndex == 0) {
        msg = "Debes seleccionar una opción";
        document.getElementById('id_error').innerHTML = msg;
        error = true;
    }

    if (activities.selectedIndex == 0) {
        msg = "Debe seleccionar una opción";
        document.getElementById('id_error').innerHTML = msg;
        error = true;
    }

    /*   if(activities.selectedIndex==4){
        
    }
    */

    if (typeComputer.selectedIndex == 0) {
        msg = "Debe seleccionar una opción";
        document.getElementById('id_error').innerHTML = msg;
        error = true;
    }

    if (timeComputer.selectedIndex == 0) {
        msg = "Debe seleccionar una opción";
        document.getElementById("id_error").innerHTML = msg;
        error = true;
    }

    if(brand.selectedIndex == 0){
        msg = "Debes seleccionar una opción";
        document.getElementById('id_error').innerHTML = msg;
        error = true;
    }else if(brand == 1){
        document.getElementById('container_modelAmd').style.display = 'block';
    }else{
        document.getElementById('container_modelAmd').style.display = 'none';
    }



    return !error;

}

let study = document.getElementsByName('study');
function compruebaEstudio() {
    if (study[0].checked == true) {
        document.getElementById('container_studies').style.display = 'block';
    } else {
        document.getElementById('container_studies').style.display = 'none';

    }
}

let computerHome = document.getElementsByName('computerH');
function compruebaOrdenador() {
    if (computerHome[0].checked == true) {
        document.getElementById('container_general').style.display = 'block';
    } else {
        document.getElementById('container_general').style.display = 'none';
    }
}

let brandComputer = document.getElementsByName('brand');

function compruebaMarca() {
    if (brandComputer[0].checked == true) {
        document.getElementById('brandContainer').style.display = 'block';
    } else {
        document.getElementById('brandContainer').style.display = 'none';
    }
}

function marca(){
    if (brand.selectedIndex == 1) {
        document.getElementById('container_modelAmd').style.display = 'block';
    } else {
        document.getElementById('container_modelAmd').style.display = 'none';
    }
}