/*
    1. Ningún campo con un (*) puede quedar vacío.
    2. El campo edad sólo puede admitir números entre 0 y 110.
    3. El campo día en la fecha de nacimiento sólo puede admitir números entre 1 y 31.
    4. El campo año en la fecha de nacimiento sólo puede admitir números entre 1900 y el 2050.
    5. La fecha de nacimiento introducida tiene que ser una fecha correcta.
    6. En el campo radio button sexo tiene que estar seleccionado uno de los dos valores(inicialmente al cargar el formulario ninguno de los dos valores está seleccionado).
    7. El valor de los dos campos de la contraseña tiene que coincidir.

    A continuación añadir un checkbox que indique que se aceptan las condiciones, y acto seguido se activaría el botón "Registrar" que inicialmente se encontrará desactivado.
 */

function validate() {

    let error = false;
    let msg = "";

    let name = document.getElementById('id_name').value;
    let surname = document.getElementById('id_surname').value;
    let email = document.getElementById('id_email').value;
    let web = document.getElementById('id_web').value;
    let age = document.getElementById('id_age').value;
    let gender = document.getElementsByName('gender');
    let day = document.getElementById('id_day').value;
    let month = document.getElementById('id_months');
    let year = document.getElementById('id_year').value;
    let user = document.getElementById('id_user').value;
    let pass1 = document.getElementById('id_pass1').value;
    let pass2 = document.getElementById('id_pass2').value;
    let submit = document.getElementById('id_submit');

    document.getElementById('error_name').innerHTML = "";
    document.getElementById('error_surname').innerHTML = "";
    document.getElementById('error_email').innerHTML = "";
    document.getElementById('error_web').innerHTML = "";
    document.getElementById('error_age').innerHTML = "";
    document.getElementById('error_sex').innerHTML = "";
    document.getElementById('error_day').innerHTML = "";
    document.getElementById('error_month').innerHTML = "";
    document.getElementById('error_year').innerHTML = "";
    document.getElementById('error_date').innerHTML = "";
    document.getElementById('error_user').innerHTML = "";
    document.getElementById('error_pass1').innerHTML = "";
    document.getElementById('error_pass2').innerHTML = "";
    document.getElementById('error_password').innerHTML = "";


    if (name.length == 0 || name == null) {
        msg = "Nombre incorrecto";
        document.getElementById('error_name').innerHTML = msg;
        error = true;
    }

    if (surname.length == 0 || surname == null) {
        msg = "Apellido incorrecto";
        document.getElementById('error_surname').innerHTML = msg;
        error = true;
    }

    if (web.length == 0 || web == null) {
        msg = "Web incorrecta";
        document.getElementById('error_web').innerHTML = msg;
        error = true;
    }

    if (age <= 0 || age >= 110) {
        msg = "Edad incorrecta";
        document.getElementById('error_age').innerHTML = msg;
        error = true;
    }

    if (gender[0].checked || gender[1].checked) {
        msg = "Seleccionado";
        document.getElementById('error_sex').innerHTML = msg;
    } else {
        msg = "Ninguno seleccionado";
        document.getElementById('error_sex').innerHTML = msg;
        error = true;
    }

    if (day <= 0 || day >= 32 || day == "") {
        msg = "Debes poner un día válido";
        document.getElementById('error_day').innerHTML = msg;
        error = true;
    }

    if (month.selectedIndex == 0) {
        msg = "Debes seleccionar un mes";
        document.getElementById('error_month').innerHTML = msg;
        error = true;
    }

    if (year <= 1900 || year >= 2050 || year == "") {
        msg = "Año fuera de los límites";
        document.getElementById('error_year').innerHTML = msg;
        error = true;
    }


    if (user.length == 0 || user == null) {
        msg = "Usuario incorrecto";
        document.getElementById('error_user').innerHTML = msg;
        error = true;
    }


    if (pass1 === pass2 && pass1 != "" && pass2 != "") {
        msg = "OK";
        document.getElementById('error_pass1').innerHTML = msg;
        document.getElementById('error_pass2').innerHTML = msg;
    } else {
        msg = "Contraseñas no coinciden o están vacias";

        document.getElementById('error_pass1').innerHTML = msg;

        msg = "Contraseñas no coinciden o están vacias";
        document.getElementById('error_pass2').innerHTML = msg;
        error = true;
    }




    return !error;
}