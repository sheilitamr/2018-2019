/* Refrescar página */
function refrescar() {
  location.reload(true);
}

/* Borrar Cookie */
function deleteCookie() {
  document.cookie = "nombre=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
  document.cookie = "usuario=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
}

/* Establecer Cookie */
function setCookie(cname, cvalue) {
  document.cookie = cname + "=" + cvalue + "; ";
}

/* Obtener cookie */
function getCookie(cname) {
  let name = cname + "=";
  let ca = document.cookie.split(';');
  for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ')
          c = c.substring(1);
      if (c.indexOf(name) == 0)
          return c.substring(name.length, c.length);
  }
  return "";
}

/* Controlar cookie */
function checkCookie() {
  let nombre = getCookie("nombre");
  let usuario = getCookie("usuario");
  if (nombre != "" && usuario != "") {
      alert("Hola " + nombre + " tu usuario es: " + usuario);
  } else {
      nombre = prompt("Da tu nombre:", "");
      if (nombre != "" && nombre != null) {
          setCookie("nombre", nombre);
      }
      usuario = prompt("Da tu usuario:", "");
      if (usuario != "" && usuario != null) {
          setCookie("usuario", usuario);
          alert("refresca la página");
      }
  }
}

/* Actualizar nombre */
function updateNombre() {
  nombre = prompt("Da tu nombre nuevamente:", "");
  if (nombre != "" && nombre != null) {
      setCookie("nombre", nombre);
  }
}

/* Actualizar usuario */
function updateUsuario() {
  usuario = prompt("Da tu usuario nuevamente:", "");
  if (usuario != "" && usuario != null) {
      setCookie("usuario", usuario);
  }
}

checkCookie();