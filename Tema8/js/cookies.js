/* CREANDO COOKIES */
    /*let user1 = document.cookie= "Usuario=Sheila";
    let user2 = document.cookie= "Usuario=Carmen";
    console.log(user2); */

/* LEER COOKIES */
    /*let read = document.cookie;
    console.log(read); */

/* CAMBIAR COOKIES */
    /*let change = document.cookie="Usuario=Pedro";
    console.log(change); */

/* ELIMINAR COOKIES */
    /*let user2 = document.cookie= "Usuario=Carmen";
    let dele = document.cookie = "Usuario=Carmen; expires=Thu, 13 DEC Jan 1970 00:00:00 UTC";
    console.log(dele);
    console.log(document.cookie); */

/* EJERCICIO */

function setCookie(cname, cvalue, exdays) {
    let d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    let expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires;
}
function getCookie(cname){
    let name = cname + "=";
    let ca = document.cookie.split(';');
    for(let i = 0; i<ca.length; i++){
        let c = ca[i];
        while(c.charAt(0)==""){
            c.substring(1);
        }
        if(c.indexOf(name)==0){
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie(){
    let user = getCookie("Username");
    if(user != ""){
        alert("Bienvenido de nuevo: "+ user);
    }else{
        user = prompt("Por favor introduzca su nombre: ","");
        if(user != "" && user != null){
            setCookie("Username", user, 365);
        }
    }

    let read = document.cookie;
    console.log(read);
}

function deleteCookie() {
    document.cookie = "nombre=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
    document.cookie = "usuario=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
}

function updateNombre() {
    nombre = prompt("Da tu nombre nuevamente:", "");
    if (nombre != "" && nombre != null) {
        setCookie("nombre", nombre);
    }
}

function updateUsuario() {
    usuario = prompt("Da tu usuario nuevamente:", "");
    if (usuario != "" && usuario != null) {
        setCookie("usuario", usuario);
    }
}