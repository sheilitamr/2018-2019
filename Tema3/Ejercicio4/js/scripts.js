function Persona(nombre, edad, telefono) {
    this.nombre = nombre;
    this.edad = edad;
    this.telefono = telefono;
}


let nPersonas;
let nombre, edad, telefono
let personas = [];

do {
    nPersonas = prompt("¿Cuántas personas quiere dar de alta?");
} while (nPersonas <= 0 || isNaN(nPersonas));

for (let i = 0; i < nPersonas; i++) {

    do {
        nombre = prompt("Introduzca el nombre completo de la persona " + (i + 1));

    } while (nombre == "" || nombre == null || nombre == " ");

    do {
        edad = parseInt(prompt("Introduzca la edad de la persona" + (i + 1)));
    } while (edad < 0 || edad > 100 || isNaN(edad) );

    do {
        telefono = prompt("Introduzca el teléfono de la persona" + (i + 1));
    } while (telefono.length != 9 || isNaN(telefono) );

    personas[i] = new Persona(nombre, edad, telefono);
}


/* Visualizar las personas cuyo nombre empieza por A*/
for (let i = 0; i < personas.length; i++) {
    if ((personas[i].nombre).charAt(0) == 'A' ) {
        console.log(personas[i].nombre + " " + personas[i].edad + " " + personas[i].telefono);
    }
} 

/* Visualizar las personas que contienen la cadena Navarro*/
for (let i = 0; i < personas.length; i++) {
    if ((personas[i].nombre).indexOf("Navarro") != -1 ) {
        console.log(personas[i].nombre + " " + personas[i].edad + " " + personas[i].telefono);
    }
} 

/* Visualizar las personas cuya edad sea mayor de 25*/
for (let i = 0; i < personas.length; i++) {
    if ((personas[i].edad)>25) {
        console.log(personas[i].nombre + " " + personas[i].edad + " " + personas[i].telefono);
    }
} 

/* Visualizar las personas que contienen la cadena Navarro*/
for (let i = 0; i < personas.length; i++) {
    if ((personas[i].telefono).splice(0,2) == "921" ) {
        console.log(personas[i].nombre + " " + personas[i].edad + " " + personas[i].telefono);
    }
} 
