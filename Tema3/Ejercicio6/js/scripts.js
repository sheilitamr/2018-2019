/* 
Escribir un programa en js que lleve a cabo lo siguiente:
    1. Dados 12 valores por teclado que representan las temperaturas máximas de los 12 meses del año, se deben verificar las siguientes propiedades para cada valor:
        *Los números deberán ser enteros y comprendidos entre el valor -10 y el valor 40.
        *Los valores de un mes no podrán variar en más o menos de 8 grados respecto al mes anterior (para el mes de febrero no hay que realizar ninguna verificación).
    2. Además de darán 12 valores que representan los litros máximos de lluvia de ese mes, cumpliendo las siguientes propiedades:
        *Los números deberán ser enteros positivos y con valores entre 0 y 50.
    3. A partir de estos requisitos, dibujar una gráfica ded las temperaturas y las lluvias. El eje vertical representa el mes y el horizontal la temperatura o la cantidad de lluvia 
    respectivamente.
*/
const meses = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];
let temperaturas = [];
let lluvias = [];

for (let i = 0; i < meses.length; i++) {

    temperaturas[i] = parseInt(prompt("Introduzca la temperatura del mes de " + meses[i]));

    if (temperaturas[i] < -10 || temperaturas[i] > 40 || isNaN(temperaturas[i])) {
        alert("Las temperaturas deben estar comprendidas entre -10 y 40");
        i--;
    }

}

for (let i = 0; i < meses.length; i++) {
    lluvias[i] = parseInt(prompt("Introduzca la precipitación del mes de " + meses[i]));

    if (lluvias[i] < 0 || lluvias[i] > 50 || isNaN(lluvias[i])) {
        alert("Las precipitaciones no pueden ser negativas y no pueden superar los 50 litros");
    }
}

document.write("<div>");
dibujarGrafica("TEMPERATURAS", temperaturas);
document.write("</div>");

document.write("<div>");
dibujarGrafica("PRECIPITACIONES", lluvias);
document.write("</div>");

function dibujarGrafica(titulo, numeros) {

    var grafica="";
    grafica=grafica+"<H1>"+titulo+"</H1><BR>";

    for (let i = 0; i < numeros.length; i++) {

        if (numeros[i] < 0) {
                        
            let aux = Math.abs(numeros[i]);

            for(let j=0; j<(10-aux);j++){
                grafica=grafica+"&nbsp;";
            }

            for (let j = 0; j < (aux); j++) {
                grafica=grafica+" = ";
            } 

            grafica=grafica+aux+ "|" + meses[i] + "|";
            
            
        }else{

            grafica=grafica+"|" + meses[i] + "|";

            for(let j=0; j<numeros[i]; j++){
                grafica=grafica+" = ";
            }

            grafica=grafica+numeros[i];
        }

        grafica=grafica+"<BR>";
    }
    window.alert("CASI");
    document.getElementById("grafica").innerHTML = grafica;
}