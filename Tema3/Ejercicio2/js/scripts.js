/* 
Solicite primero una base(2,8,10,16) en caso de no introducir una de estas bases se volverá a pedir que se introduzca la base. 
A continuación, pedirá un número comprobando que se encuentra en la base facilitada en el primer paso (en caso contrario se volverá a pedir el número) 
y a continuacion llevará a cabo la conversión de dicho número a su valor decimal con respecto a la base indicada al principio
Ejemplo: "111" "base 2"->7
*/

let base = prompt("Introduzca una base para convertir");