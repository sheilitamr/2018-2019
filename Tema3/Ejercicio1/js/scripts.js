function Libro(titulo, autor, tema) {
    this.titulo = titulo;
    this.autor = autor;
    this.tema = tema;
}

let libros;
let guardar = [];

do {
    libros = prompt("cúantos libros quieres dar de alta");

} while (libros <= 0 || isNaN(libros));

for (let i = 0; i < libros; i++) {
    let tituloL = prompt('Introduce el título del libro');
    let autorL = prompt('Introduce el autor del libro');
    let temaL = prompt('Introduce el tema del libro');

    guardar[i] = new Libro(tituloL, autorL, temaL);
}

document.write("<table border=2> <tr> <th> Titulo</th>" +
    "<th> Autor</th>" +
    "<th> Tema</th></tr>")

for (let i = 0; i < guardar.length; i++) {

    document.write("<tr> <td>" + guardar[i].titulo + " </td> <td>" + guardar[i].autor + " </td> <td>" + guardar[i].tema + "</td></tr>");

}
document.write("</table>");

