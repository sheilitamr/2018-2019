function Libro(titulo, autor, tema) {
    this.titulo = titulo;
    this.autor = autor;
    this.tema = tema;
}

let libros;
let guardar = [];
let autores = [];
let nAutores;

do {
    libros = prompt("Cuantos libros quieres dar de alta");

} while (libros <= 0 || isNaN(libros));

for (let i = 0; i < libros; i++) {
    let tituloL = prompt('Introduce el título del libro');

    let autores = [];

    do {
        nAutores = prompt('Cuantos autores tiene este libro');
    } while (nAutores < 1 || isNaN(nAutores));

    for (let i = 0; i < nAutores; i++) {
        autores[i] = prompt("Nombre del autor");
    }

    let temaL = prompt('Introduce el tema del libro');

    guardar[i] = new Libro(tituloL, autores, temaL);
}
document.write("<table border=2> <tr> <th> Titulo</th>" +
    "<th> Autor</th>" +
    "<th> Tema</th></tr>")

for (let i = 0; i < guardar.length; i++) {

    document.write("<tr> <td>" + guardar[i].titulo + " </td> <td>" + guardar[i].autor.join(",") + " </td> <td>" + guardar[i].tema + "</td></tr>");

}
document.write("</table>");

