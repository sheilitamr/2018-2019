const table = document.getElementById("table")

//delegación de eventos. Se localiza el click en el padre y se identifica al hijo donde se ha hecho click(e.target)
table.addEventListener('click', (e) => {
    if (e.target.tagName == 'A') {
        const element = e.target.parentElement.parentElement
        const nextElement = element.nextElementSibling

        console.log(element);
        console.log(nextElement);
        table.removeChild(element)
        //const auxElement = table.replaceChild(element, nextElement)
        //table.insertBefore(nextElement, element)
        //console.log(auxElement);
    }
})