//Número de enlaces de la página
const links = document.querySelectorAll('a')
console.log(links.length);
//Dirección a la que enlaza el penúltimo enlace

console.log(links[links.length-2])

//Número de enlaces que enlazan a http://prueba

let cont = 0

for (const link of links) {
    if (link.href == 'http://prueba/') cont++
}

console.log(cont);

//Número de enlaces del tercer párrafo
const paragraph = document.querySelectorAll('p')[2]

console.log(paragraph.children.length);


