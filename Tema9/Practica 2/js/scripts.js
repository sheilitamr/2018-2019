/* Número de enlaces en la página */
let enlacesP = document.getElementsByTagName("a").length;

document.getElementById("contenido").innerHTML = "El número de enlaces de la página es: " + enlacesP;

/* Dirección a la que enlaza el penúltimo enlace */

let direccion = document.getElementsByTagName("a")[document.getElementsByTagName("a").length-2];

document.getElementById("contenido").innerHTML += "<br> La dirección a la que enlaza el penúltimo enlace es: " + direccion;



/* Número de enlaces que enlazan a http://prueba */

let enlaces = 0;

for (let i = 0; i < document.getElementsByTagName("a").length; i++) {

    if (document.getElementsByTagName("a")[i].getAttributeNode("href").nodeValue == "http://prueba") {

        enlaces++;
    }
}

document.getElementById("contenido").innerHTML += "<br> El número de enlaces que enlazan a http://prueba son " + enlaces;

/* Número de enlaces del tercer párrafo. */
let parrafos = document.getElementsByTagName("p");
enlace = parrafos[2].getElementsByTagName("a").length;

document.getElementById("contenido").innerHTML += " <br> El número de enlaces del tercer párrafo son: " + enlace;