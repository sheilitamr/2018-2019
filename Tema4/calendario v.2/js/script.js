const calendar = document.getElementById("calendar");
const title = document.getElementById('calendar-title');
const calendarHead = document.getElementById('calendar-head');
const calendarDays = document.getElementById('calendar-days');
const calendarDates = document.getElementById('calendar-dates');

let date = new Date();
let currentMonth = date.getMonth();
let currentYear = date.getFullYear();

const months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
const weekDays = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];

const writeCalendar=()=>{
    title.textContent=`${months[currentMonth]} ${currentYear}`;
    const fragment=document.createDocumentFragment();

    for(let i=0; i<getFirstDay();i++){
        const span = document.createElement('SPAN');
        span.textContent='';
        span.classList.add('calendar__numbers');
        fragment.appendChild(span);

    }
    for(let i=0; i<getTotalDays();i++){
       const span = document.createElement('SPAN');
       span.textContent=i+1;

       if(date.getDate()==i+1 && date.getMonth()==currentMonth && date.getFullYear()==currentYear){
        span.classList.add('calendar__numbers--today');
       }

       span.classList.add('calendar__numbers');
       fragment.appendChild(span);
    }
    calendarDates.textContent='';
    calendarDates.appendChild(fragment)
}

const writeDays=()=>{
    const fragment = document.createDocumentFragment();
    for(const day of weekDays){
        const span = document.createElement('SPAN');
        span.textContent=day;
        span.classList.add('calendar__week');
        fragment.appendChild(span);
    }

    calendarDays.appendChild(fragment);
}

const getTotalDays = () => {

    if (currentMonth === 0 || currentMonth === 2 || currentMonth === 4 || currentMonth === 6 || currentMonth === 7 || currentMonth === 9 || currentMonth === 11) {

        return 31;

    } else if (currentMonth === 3 || currentMonth === 5 || currentMonth === 8 || currentMonth === 10) {

        return 30;
    }
    if (currentMonth == 1)
        if ((currentYear % 4 == 0) && ((currentYear % 100 != 0) || (currentYear % 400 == 0))) {
            //compruebo si el  año actual es bisiesto

            return 29;

        }

    return 28;
}

const getFirstDay = () => {
    return new Date(currentYear, currentMonth, 1).getDay()-1;
}

const previusMonth=()=> {

    currentMonth--;
    if (currentMonth == -1) {
        currentMonth = 11;
        currentYear--;
    }
    writeCalendar()
}

const nextMonth=()=> {
    currentMonth++;
    if (currentMonth == 12) {
        currentMonth = 0;
        currentYear++;
    }
    writeCalendar()
}

calendarHead.addEventListener('click',(e)=>{
    if(e.target.id=='next'){
        nextMonth();
    }else if(e.target.id=='previus'){
        previusMonth();
    }
})
writeCalendar();
writeDays();
