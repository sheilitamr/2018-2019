DROP DATABASE IF EXISTS escuela;
CREATE DATABASE escuela;
USE escuela;
-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `alumnos`
-- 

DROP TABLE IF EXISTS `alumnos`;
CREATE TABLE `alumnos` (
  `clave_alumno` mediumint(8) unsigned NOT NULL auto_increment,
  `nombre` varchar(255) NOT NULL default '',
  `edad` tinyint(3) unsigned NOT NULL default '0',
  `curso_actual` tinyint(3) unsigned NOT NULL default '0',
  PRIMARY KEY  (`clave_alumno`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='alumnos de la escuela' AUTO_INCREMENT=7 ;

-- 
-- Volcar la base de datos para la tabla `alumnos`
-- 

INSERT INTO `alumnos` VALUES (1, 'Luis Perez', 10, 2);
INSERT INTO `alumnos` VALUES (2, 'Juan Lopez', 11, 3);
INSERT INTO `alumnos` VALUES (3, 'Ana Gonzalez', 10, 3);
INSERT INTO `alumnos` VALUES (4, 'Laura Sanchez', 12, 3);
INSERT INTO `alumnos` VALUES (5, 'Antonio Ruiz', 12, 3);
INSERT INTO `alumnos` VALUES (6, 'Pedro Jimenez', 10, 2);

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `asignaturas`
-- 

DROP TABLE IF EXISTS `asignaturas`;
CREATE TABLE `asignaturas` (
  `clave_asignatura` mediumint(8) unsigned NOT NULL auto_increment,
  `nombre` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`clave_asignatura`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='asignaturas de la escuela' AUTO_INCREMENT=4 ;

-- 
-- Volcar la base de datos para la tabla `asignaturas`
-- 

INSERT INTO `asignaturas` VALUES (1, 'Historia');
INSERT INTO `asignaturas` VALUES (2, 'Biología');
INSERT INTO `asignaturas` VALUES (3, 'Matemáticas');


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `matriculas`
-- 

DROP TABLE IF EXISTS `matriculas`;
CREATE TABLE `matriculas` (
  `clave_matricula` mediumint(8) unsigned NOT NULL auto_increment,
  `clave_alumno` mediumint(8) unsigned NOT NULL default '0',
  `clave_asignatura` mediumint(8) unsigned NOT NULL default '0',
  `curso` mediumint(4) unsigned NOT NULL default '0',
  PRIMARY KEY  (`clave_matricula`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='matrículas de alumnos' AUTO_INCREMENT=10 ;

-- 
-- Volcar la base de datos para la tabla `matriculas`
-- 

INSERT INTO `matriculas` VALUES (1, 1, 1, 2016);
INSERT INTO `matriculas` VALUES (2, 1, 2, 2016);
INSERT INTO `matriculas` VALUES (3, 1, 3, 2016);
INSERT INTO `matriculas` VALUES (4, 2, 1, 2016);
INSERT INTO `matriculas` VALUES (5, 2, 2, 2016);
INSERT INTO `matriculas` VALUES (6, 3, 1, 2016);
INSERT INTO `matriculas` VALUES (7, 3, 2, 2016);
INSERT INTO `matriculas` VALUES (8, 4, 3, 2016);
INSERT INTO `matriculas` VALUES (9, 5, 2, 2016);

--