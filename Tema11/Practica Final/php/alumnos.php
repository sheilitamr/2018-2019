<?php
if(isset($_GET['asig'])){
    try{
        $asignatura = $_GET['asig'];
        $con = new PDO("mysql:host=localhost; dbname=escuela; charset=utf8", 'root', '');

        $sql= $con->prepare("SELECT alumnos.clave_alumno, alumnos.nombre, edad, curso_actual FROM alumnos, matriculas, asignaturas WHERE alumnos.clave_alumno = matriculas.clave_alumno AND matriculas.clave_asignatura = asignaturas.clave_asignatura AND asignaturas.clave_asignatura='$asignatura'");

        $sql->execute();
        $resultSet = $sql->fetchAll(PDO::FETCH_ASSOC);
        $json = json_encode($resultSet);

        echo $json;
    }catch(PDOException $e) {

        echo "Error " . $e->getMessage();
    }
}

?>