<?php

$conn = NULL;
try{

    $con = new PDO("mysql:host=localhost; dbname=escuela; charset=utf8", 'root', '');

    $sql= "SELECT nombre FROM asignaturas";

    $stm=$con->prepare($sql);

    $stm->execute();

    $resultSet = $stm->fetchAll(PDO::FETCH_ASSOC);

    $json = json_encode($resultSet);
    
    echo $json;

}catch (PDOException $e){
    echo "Error ".$e->getMessage();
}
?>