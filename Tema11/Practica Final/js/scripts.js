$(function () {
    $.get("php/asignaturas.php",function(datos){
        $.each(datos, function(key,value){
            $("#asignatura").append(`<option value=${key+1}>${value.nombre}</option>`)
            //console.log(value.nombre,key);
        })
    },"json")
    $("#mostrar").click(function(e){
        e.preventDefault()
        let val = $("#asignatura").val();
        $.getJSON(`php/alumnos.php?asig=${val}`, function(datos){
            $("#table").html("")
            $("#table").html(`<thead> <th>Nombre</th>
                                      <th>Edad</th>
                                      <th>Curso actual</th></thead>`)
            $.each(datos, function(key, value){
                $("#table").append(`<tr><td>${value.nombre}</td>
                                    <td>${value.edad}</td>
                                    <td>${value.curso_actual}</td></tr>`)
            })
            //console.log(datos[0]);
        })
    })
})

