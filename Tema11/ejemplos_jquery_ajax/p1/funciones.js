var x;
x=$(document);
x.ready(inicializarEventos);

function inicializarEventos()
{
  var x;
  x=$("#menu a");
  x.click(presionEnlace);
}

function presionEnlace()
{
  var pagina=$(this).attr("href");
  var x=$("#resultado");
  x.load(pagina);
  //la función retorna false para anular la propagación de eventos y desactivar también el evento por defecto que ocurre cuando se presiona un enlace
  return false; 
}