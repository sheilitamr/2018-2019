$(document).ready(inicializarEventos);

function inicializarEventos()
{
  var x;
  x=$("#enviar");
  x.click(presionSubmit);
}

function presionSubmit()
{
  var v=$("#nro").val();
  $.ajax({
           async:true,
           type: "POST",
           dataType: "html",
           contentType: "application/x-www-form-urlencoded; charset=UTF-8",
           url: "p5.php",
           data: "numero="+v,
           beforeSend: inicioEnvio,
           success: llegadaDatos,
           timeout: 4000,
           error: problemas
         }); 
  return false;
}

function inicioEnvio()
{
  var x=$("#resultado");
  x.html('<img src="cargando.gif">');
}

function llegadaDatos(datos)
{
  $("#resultado").html("<BR>"+datos);
}

function problemas()
{
  $("#resultado").html("<BR>"+"Problemas en el servidor.");
}