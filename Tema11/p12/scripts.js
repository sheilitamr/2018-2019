/* Desarrollar una página con dos botones. A ser presionados cambiar su color de fondo. Retornar al color originail cuando se suelta el botón del mouse */
$(function () {
    $("#button-1").mousedown(function(){
        $(this).addClass("blue")
    })
   .mouseup(function(){
       $(this).removeClass("blue")
   })

   $("#button-2").mousedown(function(){
       $(this).addClass("red")
   })
   .mouseup(function(){
       $(this).removeClass("red")
   })
    
})