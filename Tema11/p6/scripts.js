/* Desarrollar una página que contenga un hipervínculo con la propiedad href con el valor "". En la página existirán tres botones que permitan fijar distintos hipervínculos para la propiedad href del enlace anterior, modificando también el texto del hipervínculo 
Métodos:
attr(nombre_de_propiedad)
attr(nombre_de _propiedad, valor)
removeAttr(nombre_de_propiedad)*/
$(function(){
    $("#button-1").click(function(){
        $("a").attr('href','https://www.google.es/').text("Google");
    })

    $("#button-2").click(function(){
        $("a").attr('href','http://av.informaticamm.com/').text("Aula virtual");
    })

    $("#button-3").click(function(){
        $("a").attr('href','http://api.jquery.com/jQuery/').text("Api Jquery");
    })
    

})