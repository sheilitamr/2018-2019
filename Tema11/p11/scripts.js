/* Desarrollar una página que contenga un div de 200 píxeles de ancho y alto. Mostrar luego la coordenada donde se encuentra la flecha del mouse cuando está dentro del div. Mostrar un mensaje si no se encuentra dentro del div */
$(function () {
    $("div").mousemove(function(e){
        $("#content").text(`${e.pageX} , ${e.pageY}`)
    })
    $("div").mouseout(function(){
        $("#content").text("El ratón no se encuentra dentro del div")
    })
})