/* Desarrollar una página que contenga algunas palabras con el tag <strong>. Al hacer click sobre ellas ocultarlas. Asociar con jQuery el evento click a los elementos con el tag <strong> y ocultarlos con el método hide() */
$(function () {
    $("strong").click(function(){
        $("strong").hide();
    })
})