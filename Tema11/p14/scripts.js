/* Desarrollar una página que contenga dos campos de tipo texto con algún contenido. fijar de color azul su fuente. Al tomar foco el campo cambiará a color rojo. Al perder el foco, el color de la fuente del campo volverá a ser azul */
$(function () {
    $("input").focus(function(){
        $(this).toggleClass("focus")
    })
    .blur(function(){
        $(this).removeClass("focus")
    })
    
})