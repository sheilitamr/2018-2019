/* Desarrollar una página que contenga dos botones y tres párrafos (no tienen identificador ni clase asociada). El primer botón al ser presionado mostrará en una ventana con el método alert el contenido HTML de la cabecera de la página. Al presionar el segundo botón se cambiará el contenido de los dos primeros párrafos con el contenido "Jquery- práctica 8"
Métodos:
html()
html(valor) */
$(function () {
    $("#button-1").click(function(){
        alert($("head").html())
    })

    $("#button-2").click(function(){
        console.log($('p'));
        $("p:nth-child(-n+2)").html("Jquery-Práctica 8")
    })

})