/* Desarrollar una página que contenga una tabla con dos filas y dos columnas. El color de cualquier celda cambiará cando el puntero del ratón se coloca encima de dicha celda, y volverá a su color inicial cuando salimos de la celda.
Métodos:
mouseover()
mouseover(function)
mouseout()
mouseout(function) */
$(function () {
    $("table td").mouseover(function () { 
        $(this).addClass("blue");
   });

    $("table td").mouseout(function () { 
        $(this).removeClass("blue");
   });
})