/* Desarrollar una página que contenga un combobox con cuatro ítems y ocho botones que lleven a cabo las siguientes acciones:
Botón1: Eliminar la lista completa
Botón2: Restaurar la lista completa
Botón3: Añadir un elemento al final de la lista
Botón4: Añadir un elemento al principio de la lista
Botón5: Eliminar el último elemento
Botón6: Eliminar el primer elemento
Botón7: Eliminar el primero y segundo elemento
Botón8: Eliminar los dos últimos */
$(function () {
    $("#removeList").click(function(){
        $("select option").each(function(i){
            $(this).remove()
        })
    })

    $("#restoreList").click(function(){
        $("select").append(`<option value='volvo'>Volvo</option>
        <option value='saab'>Saab</option>
        <option value='fiat'>Fiat</option>
        <option value='audi'>Audi</option>`)
    })

    $("#addEnd").click(function(){
        $("select").append(`<option value='citroen'>Citroën</option>`)
    })

    $("#addFirst").click(function(){
        $("select").prepend(`<option value='chevrolet'>Chevrolet</option>`)
    })

    $("#removeLast").click(function(){
        $("select option:last-child").remove()
    })

    $("#removeFirst").click(function(){
        $("select option:first-child").remove()
    })

    $("#removeTwoFirst").click(function(){
        $("select option:nth-child(-n+2)").remove()
    })

    $("#removeTwoLast").click(function(){
        $("select option:nth-last-child(-n+2)").remove()
    })

})