/* Desarrollar una página que contenga dos div con un conjunto de párrafos cada uno. Al cargar la página sólo aparecerá visible el primer div. Cuando se presione con el mouse sobre este div se ocultará lentamente, y cuando esté lentamente, y cuando esté completamente oculto aparecerá de forma lenta el segundo div. Al pulsar con el ratón sobre el segundo div, se reducirá su opacidad a 0.20 */
$(function () {
  $(window).ajaxStart(function(){
      $("#box").html("<img src='load.gif'>");
  })

  $("a").click(function(e){
      e.preventDefault()
      $.ajax({
          url:$(this).attr('href') + '.html',
          success:function(data){
              console.log(data);
              $("#box").html(data)
          }
      })
  })
})