/* 
    1. Introducir un número
        1.1 Pulso el botón y tiene que aparecer en la pantalla
    2. Introducir un símbolo
        1.1 Guardar el primer número
        1.2 Imprimir símbolo
    3. Introducir otro número
        1.1 Guardar símbolo
        1.2 Imprimir número2
    4. Y pulsar en = para saber el resultado
        4.1 Guardar número 2
        4.2 Hacer operacion

*/

const screenOperation = document.getElementById('screen-operation');
const screenResult = document.getElementById('screen-result');
const buttons = document.getElementById('calculator-buttons');
/* let number1;
let number2; */
/* let operationType; */
let operation = false;

/* const lastValue = () => {
    screenOperation.textContent.substring(screenOperation.textContent.length - 1);
} */

const writeOperation = text => {
    if (screenOperation.textContent == 0) {
        screenOperation.textContent = '';
    }
    screenOperation.textContent += text;

    if (operation && isNaN(text)) {
        screenOperation.textContent = screenResult.textContent;
        operation = false;
    }

    if (operation && !isNaN(text)) {
        screenOperation.textContent = '';
        screenResult.textContent = '0';
        operation = false;
    }

    /*   if (isNaN(lastValue()) && isNaN(text)) {
          screenOperation.textContent = screenOperation.textContent.substring(0, screenOperation.textContent.length - 1);
      } else {
  
          screenOperation.textContent += text;
      } */

}

const writeResult = () => {
    screenResult.textContent = eval(screenOperation.textContent);
    operation = true;

}

const resetScreen = () => {
    screenOperation.textContent='0';
    screenResult.textContent='0';
}

buttons.addEventListener('click', (e) => {

    if (e.target.textContent !== '') {
        switch (e.target.textContent) {
            case '=': writeResult(); break;
            case 'C': resetScreen(); break;
            case ',': writeOperation('.'); break;
            default: writeOperation(e.target.textContent); break;
        }
    }
});

