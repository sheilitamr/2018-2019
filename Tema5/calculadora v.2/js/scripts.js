/* 
    1. Introducir un número
        1.1 Pulso el botón y tiene que aparecer en la pantalla
    2. Introducir un símbolo
        1.1 Guardar el primer número
        1.2 Imprimir símbolo
    3. Introducir otro número
        1.1 Guardar símbolo
        1.2 Imprimir número2
    4. Y pulsar en = para saber el resultado
        4.1 Guardar número 2
        4.2 Hacer operacion

*/

const screen = document.getElementById('calculator-screen');
const buttons = document.getElementById('calculator-buttons');
let number1;
let number2;
let operationType;
let operation = false;

buttons.addEventListener('click', (e) => {
    if (e.target.dataset.number) {
        writeNumber(e);
    } else if (e.target.dataset.operation) {
        if (!operation) {
            number1 = screen.textContent;
        }
        writeOperation(e);

        if (e.target.dataset.operation == '=') {
            result();
        }

    }
});

const writeNumber = (e) => {
    if(screen.textContent == 0){
        screen.textContent='';
    }
    screen.textContent += e.target.dataset.number;
}

const writeOperation = (e) => {

    screen.textContent += e.target.dataset.operation;
    operationType = screen.textContent.substring(screen.textContent.length - 1);
    operation = true;
}

const result = () => {

    number2 = screen.textContent.substring(screen.textContent.indexOf(operationType) - 1, screen.textContent.length - 1);

    console.log(number1, number2, operationType);
    getResult();
}

/* const writeResult=()=>{
    screen.textContent=eval(screen.textContent);
} */

 const getResult=()=>{
    switch(operationType){
        case '+':
            screen.textContent += number1+number2;
            break;
        case '/':
            screen.textContent += number1/number2;
    }
} 
