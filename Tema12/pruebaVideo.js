
var reproductor = document.getElementById("reproductor");

reproductor.addEventListener('play', function () {
    window.alert("Empieza la reproducción");
}, false);

function reproducir_pausar() {
    var bReproductor = document.getElementById("bReproductor");


    if (reproductor.paused) {
        bReproductor.value = "Detener";
        reproductor.play();
    } else {
        bReproductor.value = "Reproducir";
        reproductor.pause();
    }
}