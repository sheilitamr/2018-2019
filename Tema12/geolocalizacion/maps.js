let erroresDiv = document.getElementById("errores");

function obtenerLocalizacion() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(DibujarMapa, MostrarErrores);
    } else {
        erroresDiv.innerHTML = "Geolocalización no soportada por el navegador";
    }
}

function DibujarMapa(position) {
    var pos_lat = position.coords.latitude;
    var pos_lng = position.coords.longitude;
   
    myLatlng = {
        Lat: pos_lat,
        Lng: pos_lng
    }

    var mapOptions = {
        zoom: 15,
        center: new google.maps.LatLng(myLatlng.Lat, myLatlng.Lng),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var my_map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

    var marker = new google.maps.Marker({
        position: { lat: pos_lat, lng: pos_lng },
        map: my_map
    });

    var infowindow = new google.maps.InfoWindow({
        content: '<p>Marker Location:' + marker.getPosition() + '</p>'
    });
    google.maps.event.addListener(marker, 'click', function () {
        infowindow.open(my_map, marker);
    });

}
function MostrarErrores(error) {
    switch (error.code) {
        case error.PERMISSION_DENIED:
            erroresDiv.innerHTML = "El usuario denegó la petición de geolocalización";
            break;
        case error.POSITION_UNAVAILABLE:
            erroresDiv.innerHTML = "Información de localización no disponible";
            break;
        case error.TIMEOUT:
            erroresDiv.innerHTML = "La petición para obtener la ubicación del usuario expiró";
            break;
        case error.UNKNOW_ERROR:
            erroresDiv.innerHTML = "Error desconocido";
    }
}
obtenerLocalizacion();